#include "h_greedy_relaxed_plan.h"

#include "../option_parser.h"
#include "../plugin.h"

using namespace std;

namespace planopt_heuristics {
GreedyRelaxedPlanHeuristic::GreedyRelaxedPlanHeuristic(const options::Options &options)
    : Heuristic(options),
      relaxed_task(task_proxy) {
}

int GreedyRelaxedPlanHeuristic::compute_heuristic(const GlobalState &state) {
    relaxed_task.initial_state = relaxed_task.translate_state(state);

    vector<bool> proposition_reached(relaxed_task.propositions.size(), false);
    vector<bool> operator_used(relaxed_task.operators.size(), false);
    for (int id : relaxed_task.initial_state) {
        proposition_reached[id] = true;
    }

    bool isGoal = true;
    for (PropositionID id : relaxed_task.goal){
        if (!proposition_reached[id]) isGoal = false;
    }
    if (isGoal){
        return 0;
    }

    bool added_propositions;
    bool appliable;
    bool may_apply;
    int cost = 0;
    do{

        added_propositions = false;
        
        for (RelaxedOperator op : relaxed_task.operators){
            appliable = true;
            for (int id: op.preconditions){
                if (! proposition_reached[id]) appliable = false;
            }
            if (appliable){
                may_apply = false;
                for (int id: op.effects){ 
                    if (! proposition_reached[id] ){
                        // if this proposition was not reached, than the operator is adding it and will be applied
                        may_apply = true;
                        break;
                    }
                }
                if (may_apply){
                    for (int id: op.effects){
                        proposition_reached[id] = true;
                    }
                    cost+=1;
                    isGoal = true;
                    for (PropositionID id : relaxed_task.goal){
                        if (!proposition_reached[id]) isGoal = false;
                    }
                    if (isGoal){
                        return cost;
                    }
                    operator_used[op.id] = true;
                    added_propositions = true;
                    break;
                    
                }
            }
        }

    }while (added_propositions);

    // TODO: add your code for exercise 1 (c) here.
    return DEAD_END;
}

static Heuristic *_parse(OptionParser &parser) {
    Heuristic::add_options_to_parser(parser);
    Options opts = parser.parse();
    if (parser.dry_run())
        return 0;
    else
        return new GreedyRelaxedPlanHeuristic(opts);
}

static Plugin<Heuristic> _plugin("planopt_greedy_relaxed", _parse);

}
