#include "relaxed_task_graph.h"

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

namespace planopt_heuristics {
RelaxedTaskGraph::RelaxedTaskGraph(const TaskProxy &task_proxy)
    : relaxed_task(task_proxy),
      variable_node_ids(relaxed_task.propositions.size()) {
    /*
      TODO: add your code for exercise 2 (b) here. Afterwards
        - variable_node_ids[i] should contain the node id of the variable node for variable i
        - initial_node_id should contain the node id of the initial node
        - goal_node_id should contain the node id of the goal node
        - the graph should contain precondition and effect nodes for all operators
        - the graph should contain all necessary edges.
    */

    // Add an OR node for each state variable
    for (Proposition p : relaxed_task.propositions){
        variable_node_ids[p.id] = graph.add_node(NodeType::OR);
    }
    // Add AND node for initial state
    initial_node_id = graph.add_node(NodeType::AND);
    // Add an initial state arc from each variable node
    // in initial state to initial node
    for (PropositionID id : relaxed_task.initial_state){
        graph.add_edge(variable_node_ids[id], initial_node_id);
    }
    // Add AND node for goal state
    goal_node_id = graph.add_node(NodeType::AND);
    // Add a goal state arc from goal node to each variable node in goal
    for (PropositionID id : relaxed_task.goal){
        graph.add_edge(goal_node_id, variable_node_ids[id]);
    }
    // For each operator, add an AND node
    for (RelaxedOperator o : relaxed_task.operators){
        NodeID newOperatorNode = graph.add_node(NodeType::AND, o.cost);
        // Add arcs from this AND node to variable nodes
        // that are preconditions
        for (PropositionID pre : o.preconditions){
            for (int i = 0; i < (int)relaxed_task.propositions.size(); i++){
                if(pre == relaxed_task.propositions[i].id){
                    graph.add_edge(newOperatorNode, variable_node_ids[pre]);
                }
            }
        }
        // Add arcs from each variable that is an effect to this AND node
        for (PropositionID eff : o.effects){
            for (int i = 0; i < (int)relaxed_task.propositions.size(); i++){
                if(eff == relaxed_task.propositions[i].id){
                    graph.add_edge(eff, newOperatorNode);
                }
            }
        }
    }
}

void RelaxedTaskGraph::change_initial_state(const GlobalState &global_state) {
    // Remove all initial edges that where introduced for relaxed_task.initial_state.
    for (PropositionID id : relaxed_task.initial_state) {
        graph.remove_edge(variable_node_ids[id], initial_node_id);
    }

    // Switch initial state of relaxed_task
    relaxed_task.initial_state = relaxed_task.translate_state(global_state);

    // Add all initial edges for relaxed_task.initial_state.
    for (PropositionID id : relaxed_task.initial_state) {
        graph.add_edge(variable_node_ids[id], initial_node_id);
    }
}

bool RelaxedTaskGraph::is_goal_relaxed_reachable() {
    // Compute the most conservative valuation of the graph and use it to
    // return true iff the goal is reachable in the relaxed task.

    // TODO: add your code for exercise 2 (b) here.
    graph.most_conservative_valuation();
    //The relaxed task is solvable if the goal node is forced true
    return graph.get_node(this->goal_node_id).forced_true;
}

int RelaxedTaskGraph::additive_cost_of_goal() {
    // Compute the weighted most conservative valuation of the graph and use it
    // to return the h^add value of the goal node.

    // TODO: add your code for exercise 2 (c) here.
    graph.weighted_most_conservative_valuation();
    return graph.get_node(this->goal_node_id).additive_cost;
}

int RelaxedTaskGraph::ff_cost_of_goal() {
    // TODO: add your code for exercise 2 (e) here.
    int cost = 0;
    unordered_set<NodeID> achievers;
    graph.weighted_most_conservative_valuation();
    collect_achievers(achievers, graph.get_node(this->goal_node_id));
    for (NodeID achiever : achievers){
        cost = cost + graph.get_node(achiever).direct_cost;
    }
    return cost;
}


void RelaxedTaskGraph::collect_achievers(std::unordered_set<NodeID> &achievers, AndOrGraphNode node) {

    if(node.type == NodeType::AND){
        for (NodeID id : node.successor_ids){
            achievers.insert(node.id);
            collect_achievers(achievers, graph.get_node(id));
        }
    } else {
         achievers.insert(node.id);
         collect_achievers(achievers, graph.get_node(node.achiever));
    }
}

}
